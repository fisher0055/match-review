import { writable } from 'svelte/store';

function createMatchesStore() {
	const { subscribe, set, update } = writable([]);

	return {
		subscribe,
		load: (matches) => {
			return update(n => n = matches);
		},
		reset: () => set([])
	};
}

export const matchesStore = createMatchesStore();