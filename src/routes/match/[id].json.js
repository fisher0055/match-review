import { database } from '../../database';

const lookup = new Map();

export const get = (req, res, next) => {
	// the `id` parameter is available because
	// this file is called [id].json.js
	const { id } = req.params;

	const matchRef = database.collection('matches').doc(id);

	matchRef.get().then((doc) => {
		if (doc.exists) {
			res.writeHead(200, {
				'Content-Type': 'application/json'
			});

			console.log('docData', doc.data());

			res.end(doc.data());
		}
	}).catch(e => {
		res.writeHead(404, {
			'Content-Type': 'application/json'
		});
		res.end(JSON.stringify({
			message: e
		}));
	});

	if (lookup.has(id)) {
		res.writeHead(200, {
			'Content-Type': 'application/json'
		});

		res.end(lookup.get(id));
	} else {
		res.writeHead(404, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			message: `Not found`
		}));
	}
};
