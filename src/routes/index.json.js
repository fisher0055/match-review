import { database } from '../database';

export const get = async (req, res, next) => {
  const matchesList = [];
  const matchesRef = database.collection('matches');

  await matchesRef.get().then(querySnapshot => {
    querySnapshot.forEach(doc => {
      if (doc.exists) {
        const matchData = doc.data();
        matchesList.push(matchData);
      }
    });
  });

  if (matchesList.length > 0) {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    
    res.end(JSON.stringify(matchesList));
  } else {
    res.writeHead(404, {
      'Content-Type': 'application/json'
    });

    res.end(JSON.stringify({
      error: 'NO MATCHES'
    }));
  }
};
