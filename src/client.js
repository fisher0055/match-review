import * as sapper from '@sapper/app';

sapper.start({
	target: document.querySelector('#sapper')
}).then(() => {
	console.log('APP STARTED');
}).catch(e => {
	console.error(e);
});