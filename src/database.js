const firebase = require('firebase');

firebase.initializeApp({
  apiKey: 'AIzaSyBan_xMzEMWID3y7b_LWEgAfnrK8EJk9Xo',
  authDomain: 'tactics-pro.firebaseapp.com',
  databaseURL: 'https://tactics-pro.firebaseio.com',
  projectId: 'tactics-pro',
  storageBucket: 'tactics-pro.appspot.com',
  messagingSenderId: '993536832567',
  appId: '1:993536832567:web:41b8c4da66a36454c8fbb2',
  measurementId: 'G-4JWHF3TVC1',
});

export const database = firebase.firestore();